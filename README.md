Projeto Servidor Go com Redis
Este é um projeto simples de um servidor Go que utiliza o framework Fiber e o Redis para gerenciamento de sessões.

Como executar o projeto
Construa a imagem Docker:
docker build -t my-app .
Copy
Execute a imagem em um contêiner Docker:
docker run -p 8080:8080 my-app
Copy
Dependências
O projeto tem as seguintes dependências:

Fiber: Um framework web Go inspirado no Express.js.
Redis: Um cliente Redis para Go.
Rotas
O servidor define as seguintes rotas:

GET /: Retorna uma mensagem “Hello, world!”.
POST /: Verifica se o nome de usuário e a senha fornecidos correspondem às credenciais do administrador. Se corresponderem, uma nova sessão é criada no Redis.
GET /exist/:session: Verifica se uma sessão com o UUID fornecido existe no Redis.
