# Use a imagem oficial do Go
FROM golang:1.20-alpine

# Crie um diretório de trabalho
WORKDIR /app

# Copie os arquivos go.mod e go.sum
COPY go.mod go.sum ./

# Baixe todas as dependências
RUN go mod download

# Copie o código fonte para o contêiner
COPY . .

# Compile o aplicativo
RUN go build -o main .

# Exponha a porta em que o aplicativo será executado
EXPOSE 3000

# Comando para iniciar o aplicativo
CMD ["./main"]
